<?php
use Luracast\Restler\RestException;

/**
 * Class Otro
 *
 */
class Otro
{
    public function get()
    {
        try {
            return 'otro';
        } catch (\Exception $e) {
            throw new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage());
        }
    }
    
    public function delete()
    {
    	try {
    		return 'otro';
    	} catch (\Exception $e) {
    		throw new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage());
    	}
    }
}