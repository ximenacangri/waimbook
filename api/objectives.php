<?php
use Luracast\Restler\RestException;
include_once("model/objectives.php");

/**
 * Class Objectives
 *
 */
class Objectives
{
	/**
	 * @url GET
	 */
    public function getObjectives()
    {        
        try{
        	$obj = new objectivesModel();
        	$resp = $obj->getAllObj();
        	return $resp;
        } catch (\Exception $e){
        	//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
        	return $e->getMessage();
        }
    }
    
    /**
     * @url GET /:obj_uid
     *
     * @param string $obj_uid {@min 15}{@max 32}
     */
    public function getObjective($obj_uid)
    {
    	try{
    		$obj = new objectivesModel();
    		$resp = $obj->getObj($obj_uid);
    		return $resp;
    	} catch (\Exception $e){
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url POST
     *
     * @param array $request_data
     *
     * @status 201
     */
    public function postObjective($request_data)
    {
    	try {
    		$obj = new objectivesModel();
    		$arrayData = $obj->createObj($request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url PUT /:obj_uid
     *
     * @param string $obj_uid      {@min 15}{@max 32}
     * @param array  $request_data
     */
    public function putObj($obj_uid, $request_data)
    {
    	try {
    		$obj = new objectivesModel();
    		$arrayData = $obj->updateObj($obj_uid, $request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url DELETE /:obj_uid
     *
     * @param string $obj_uid {@min 15}{@max 32}
     */
    public function deleteObj($obj_uid)
    {
    	try {
    		$obj = new objectivesModel();
    		$obj->deleteObj($obj_uid);
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    

    /**
     * @url GET /:usr_uid
     *
     * @param string $usr_uid
     */
    public function getUserObjectives($usr_uid)
    {
    	try{
    		$obj = new objectivesModel();
    		$resp = $obj->getObj($usr_uid);
    		return $resp;
    	} catch (\Exception $e){
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
}