<?php
use Luracast\Restler\RestException;
include_once("model/tasks.php");

/**
 * Class Tasks
 *
 */
class Tasks
{
	/**
	 * @url GET
	 */

    
    /**
     * @url GET /:usr_uid
     *
     * @param string $usr_uid {@min 15}{@max 32}
     */
    public function getTask($tsk_uid)
    {
        die('HELLO');
    	try{
    		$tasks = new tasksModel();
    		$resp = $tasks->getTask($tsk_uid);
    		//$foo = array('bar' => 'baz');
    		return $resp;
    	} catch (\Exception $e){
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    public function getObjTask($obj_uid)
    {
        die('HELLO');
        try{
            $tasks = new tasksModel();
            $resp = $tasks->getObjTask($obj_uid);
            //$foo = array('bar' => 'baz');
            return $resp;
        } catch (\Exception $e){
            //throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
            return $e->getMessage();
        }
    }
    
    /**
     * @url POST
     *
     * @param array $request_data
     *
     * @status 201
     */
    public function postTask($request_data)
    {
    	try {
    		$task = new taskModel();
    		$arrayData = $task->createTask($request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url PUT /:usr_uid
     *
     * @param string $usr_uid      {@min 15}{@max 32}
     * @param array  $request_data
     */
    public function putTask($tsk_uid, $request_data)
    {
    	try {
    		//$userLoggedUid = $this->getUserId();
    		$task = new taskModel();
    		//$arrayData = $user->update($usr_uid, $request_data, $userLoggedUid);
    		$arrayData = $task->update($tsk_uid, $request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url DELETE /:usr_uid
     *
     * @param string $usr_uid {@min 15}{@max 32}
     */
    public function deleteTask($tsk_uid)
    {
    	try {
    		$task = new taskModel();
    		$task->deleteTask($tsk_uid);
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    
    /**
     * @url GET /:usr_username/:usr_password
     *
     * @param string usr_username 
     * @param string usr_password {@min 6}{@max 32} 
     */
}