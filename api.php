<?php
use Luracast\Restler\Restler;
use Luracast\Restler\Defaults;

require_once 'vendor/restler.php';
$dir = opendir("./api");
$classes = array();
while ($file = readdir($dir))
{
    if (!is_dir($file))
    {
        require_once '/api/' . $file;
        $classes[] = str_replace('.php', '', $file);
    }
}

// Defaults::$throttle = 20; //time in milliseconds for bandwidth throttling

$r = new Restler();
foreach ($classes as $value) {
    $r->addAPIClass($value); // repeat for more
}
$r->handle(); //serve the response