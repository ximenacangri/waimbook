<?php 
include_once("connection.php");

/**
 * Class users
 *
 */
class usersModel
{

	public function getAll()
	{
		/*$queEmp = "SELECT * FROM users ORDER BY usr_username ASC";
		$resEmp = mysql_query($queEmp) or die(mysql_error());
		$totEmp = mysql_num_rows($resEmp);	
		return $resEmp;*/
		$query = "SELECT * FROM users ORDER BY usr_username ASC";
		$res = executeQuery($query);
		
		return $res;
	}
	
	public function getUser($useruid)
	{
		if($useruid == ""){
			throw new \Exception("The user_uid is needed.");
		} else {
			$query = "SELECT * FROM users WHERE usr_uid = '" . $useruid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
	
	public function testPassword ($sPassword = '')
	{
		$aFields = array();
		$dateNow = date('Y-m-d H:i:s');
		
		if ($sPassword == "") {
			$aFields['MESSAGE'] = "The password is empty";
			$aFields['STATUS'] = 0;
		} else {
			if(strlen($sPassword) < 6){
				$aFields['MESSAGE'] = "The password is to short, at least 6 characters are needed";
				$aFields['STATUS'] = 0;
			} else {
				$aFields['MESSAGE'] = "ok";
				$aFields['STATUS'] = 1;
			}
		}
		return $aFields;
	}
	
	public function createUser($arrayData)
	{
		$arrayData = array_change_key_case($arrayData, CASE_UPPER);		
		$form = $arrayData;
		$data = array();
		
		// A uniqid, like: 002515wnsm5ss95
		$data['USR_UID'] = uniqid('00');
		
		if($form['USR_USERNAME'] == ''){
			throw new \Exception("A username is necessary.");
		} else {
			$query = "SELECT * FROM users WHERE usr_username = '" . $form['USR_USERNAME'] . "'";
			$res = executeQuery($query);

			if (count($res) >= 1){
				throw new \Exception("The username '" . $form['USR_USERNAME'] . "' already exist!");
			} else {
				$data['USR_USERNAME'] = $form['USR_USERNAME'];
			}			
		}
		
		$sConfirm = $this->testPassword($form['USR_PASSWORD']);

		if($sConfirm['STATUS'] != 1){
			throw new \Exception($sConfirm['MESSAGE']);
		} else {
			$data['USR_PASSWORD'] = md5($form['USR_PASSWORD']);
		}
		
		if($form['USR_FIRSTNAME'] == ''){
			throw new \Exception("A first name is necessary.");
		} else {
			$data['USR_FIRSTNAME'] = $form['USR_FIRSTNAME'];
		}
		
		if($form['USR_LASTNAME'] == ''){
			throw new \Exception("A lastname is necessary.");
		} else {
			$data['USR_LASTNAME'] = $form['USR_LASTNAME'];
		}
		
		if($form['USR_EMAIL'] == ''){
			throw new \Exception("A email is necessary.");
		} else {
			$data['USR_EMAIL'] = $form['USR_EMAIL'];
		}
		
		$data['USR_CREATE_DATE'] = date('Y-m-d H:i:s');
		$data['USR_STATUS'] = "1";		
		
		$query = "INSERT INTO users (usr_uid, usr_username, usr_password, usr_firstname, usr_lastname, usr_email, usr_create_date, usr_status) 
		VALUES ('" . $data['USR_UID'] . "', '" . $data['USR_USERNAME'] . "', '" . $data['USR_PASSWORD'] . "', '" . $data['USR_FIRSTNAME'] . "', '" . 
		$data['USR_LASTNAME'] . "', '" . $data['USR_EMAIL'] . "', '" . $data['USR_CREATE_DATE'] . "', '" . $data['USR_STATUS'] . "')"; 
		
		//print_r($query);
		//die('hello');
		$res = executeQuery($query);
		
		return $res;
	}
	
	public function updateUser($usr_uid, $data)
	{
		
	}
	
	public function deleteUser($usr_uid)
	{
		if($usr_uid == ""){
			throw new \Exception("The user_uid is needed.");
		} else {
			$query = "DELETE FROM users WHERE usr_uid = '" . $usr_uid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
	
	public function login($usr_username = "", $usr_pass = "")
	{
		if($usr_username != ""){
			$query = "SELECT usr_password, usr_uid FROM users WHERE usr_username = '" . $usr_username . "'";
			$res = executeQuery($query);
			
			if($res != ""){
				$resp = $res[0];
				$pass =  $resp->usr_password;
				$uid = $resp->usr_uid;
				
				$usr_pass_encripted = md5($usr_pass);
				
				if ($usr_pass_encripted == $pass){
					$res = array("status"=>"1", "usr_uid"=>$uid);
					return $res;
				} else {
					throw new \Exception("The username and password are incorrect.");
				}
				
			} else {
				throw new \Exception("The username dosen't exist.");
			}
			
		} else {
			throw new \Exception("The username is empty.");
		}
	}
}

?>

