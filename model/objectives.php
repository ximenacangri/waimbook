<?php 
include_once("connection.php");

/**
 * Class objectivesModel
 *
 */
class objectivesModel
{

	public function getAllObj()
	{
		$query = "SELECT * FROM objectives ORDER BY obj_date_ini ASC";
		$res = executeQuery($query);
		
		return $res;
	}
	
	public function getObj($obj_uid)
	{
		$query = "SELECT * FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
		$res = executeQuery($query);
	
		return $res;
	}
	
	public function createObj($arrayData)
	{
		$arrayData = array_change_key_case($arrayData, CASE_UPPER);		//mayusculas
		$form = $arrayData;
		$data = array();
		
		// A uniqid, like: 002515wnsm5ss95
		$data['OBJ_UID'] = uniqid('00');
		
		if($form['OBJ_TITLE'] == ''){
			throw new \Exception("Objective tittle needed.");
		} else {
		    $data['OBJ_TITLE'] = $form['OBJ_TITLE'];
		}
		
		$data['OBJ_DESCRIPTION'] = $form['OBJ_DESCRIPTION'];
		
		if($form['OBJ_DATE_INI'] == ''){
			throw new \Exception("An start objective date is needed.");
		} else {
			$data['OBJ_DATE_INI'] = $form['OBJ_DATE_INI'];
		}
		
		if($form['OBJ_DATE_END'] == ''){
			throw new \Exception("An ending objective date is needed.");
		} else {
			$data['OBJ_DATE_END'] = $form['OBJ_DATE_END'];
		}
		
		if($form['OBJ_DAYS'] == ''){
			throw new \Exception("The days to do this objective are needed.");
		} else {
			$data['OBJ_DAYS'] = $form['OBJ_DAYS'];
		}
				
		if($form['OBJ_DAYTIME_INI'] == ''){
			throw new \Exception("An start time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_INI'] = $form['OBJ_DAYTIME_INI'];
		}
		
		if($form['OBJ_DAYTIME_END'] == ''){
			throw new \Exception("An ending time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_END'] = $form['OBJ_DAYTIME_END'];
		}
		
		if($form['OBJ_ALARM_STATUS'] == ''){
			$data['OBJ_ALARM_STATUS'] = "0";
		} else {
			$data['OBJ_ALARM_STATUS'] = $form['OBJ_ALARM_STATUS'];
		}
		
	    if( $data['OBJ_ALARM_STATUS'] == 1){
	    	if($form['OBJ_ALARM_TIME'] == ''){  //Alarma Date
	    		throw new \Exception("The alarm time before the objective is needed.");
	    	} else {
	    		$data['OBJ_ALARM_TIME'] = $form['OBJ_ALARM_TIME'];
	    	}
	    } else {
	    	$data['OBJ_ALARM_TIME'] = "";
	    }		
		
	    if($form['USR_UID'] == ''){
	    	throw new \Exception("The user uid for the objective is needed.");
	    } else {
	    	$data['USR_UID'] = $form['USR_UID'];
	    }
		
		$query = "INSERT INTO objectives (obj_uid, obj_title, obj_description, obj_date_ini, obj_date_end, obj_days, obj_daytime_ini, obj_daytime_end, obj_alarm_status, obj_alarm_time, usr_uid) 
		VALUES ('" . $data['OBJ_UID'] . "', '" . $data['OBJ_TITLE'] . "', '" . $data['OBJ_DESCRIPTION'] . "', '" . $data['OBJ_DATE_INI'] . "', '" . 
		$data['OBJ_DATE_END'] . "', '" . $data['OBJ_DAYS'] . "', '" . $data['OBJ_DAYTIME_INI'] . "', '" . $data['OBJ_DAYTIME_END'] . "', '" . $data['OBJ_ALARM_STATUS'] . "', '" . $data['OBJ_ALARM_TIME'] . "', '" . $data['USR_UID'] . "')"; 
		
		//print_r($query);
		//die('hello');
		$res = executeQuery($query);
		
		return $res;
	}

	public function deleteObj($obj_uid)
	{
		if($obj_uid == ""){
			throw new \Exception("The obj_uid is needed.");
		} else {
			$query = "DELETE FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
	
	public function getUserObjectives($usr_uid){
		if($usr_uid == ""){
			throw new \Exception("The usr_uid is needed to list his objectives.");
		} else {
			$query = "SELECT * FROM objectives WHERE usr_uid = '" . $usr_uid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
	
}
?>

