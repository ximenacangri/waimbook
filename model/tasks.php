<?php 
include_once("connection.php");

/**
 * Class tasks
 *
 */
class tasksModel
{	
	public function getTask($tsk_uid)
	{	
		$query = "SELECT * FROM tasks WHERE tsk_uid = '" . $tsk_uid . "'";
		$res = executeQuery($query);
	
		return $res;
	}
	public function getObjTasks($obj_uid)
	{
		$query = "SELECT * FROM users WHERE obj_uid = '" . $form['OBJ_UID'] . "'";
		$res = executeQuery($query);
		return $res;
	} //
	
	public function createTask($arrayData)
	{
		$arrayData = array_change_key_case($arrayData, CASE_UPPER);		
		$form = $arrayData;
		$data = array();
		// A uniqid, like: 002515wnsm5ss95
		$data['TSK_UID'] = uniqid('00');
		
		if($form['TSK_TITLE'] == ''){
			throw new \Exception("A title is necessary.");
		} else {
				$data['TSK_TITLE'] = $form['TSK_TITLE'];
		}				
		if($form['OBJ_UID'] == ''){
			throw new \Exception("An objective id is necessary.");
		} else {
			$data['OBJ_UID'] = $form['OBJ_UID'];
		}
		
		if($form['TSK_STATUS'] ==''){
			$data['TSK_STATUS'] = "0";	
		} else{
			$data['TSK_STATUS'] = $form['TSK_STATUS'];
		}
		
		$query = "INSERT INTO tasks (tsk_uid, tsk_title, tsk_status, obj_uid) 
		VALUES ('" . $data['TSK_UID'] . "', '" . $data['TSK_TITLE'] . "', '" . $data['TSK_STATUS'] . "', '" . $data['OBJ_UID'] . "')"; 
		$res = executeQuery($query);
		return $res;
	}
	public function deleteTask($tsk_uid)
	{
		if($tsk_uid == ""){
			throw new \Exception("The tsk_uid is needed.");
		} else {
			$query = "DELETE FROM tasks WHERE tsk_uid = '" . $usr_uid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
}
?>

